from django.db import models
from datetime import date
from django.utils.html import format_html
# Create your models here.

class Task(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()
    created_date = models.DateField(auto_now_add=True)
    due_date = models.DateField(auto_now_add=False)
    closed = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def colored_due_date(self):
        due_date = django_date(self.due_date, "d F Y")
        return format_html("<span style=color:%s>%s</span>"%(color_due_date))

    colored_due_date.allows_tag = True

class AddTask(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    due_date = models.DateField(blank=True, null=False)
