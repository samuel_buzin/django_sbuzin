from django.conf.urls import url, include
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^home/(\w+)$', views.home_name, name='home_name'),
    url(r'^home$', views.home, name='home'),
    url(r'^$', views.home, name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^listing$', views.task_listing, name="listing"),
    url(r'^addtask/$', views.addtask, name='addtask')
]
