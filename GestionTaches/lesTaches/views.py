from django.shortcuts import render
from django.forms import ModelForm, Textarea
from django.http import HttpResponse, HttpResponseRedirect
from django.db import models
from django import forms
from .models import Task, AddTask
from django.urls import reverse

class TaskForm(ModelForm):
    class Meta:
        model = AddTask
        fields = ('name', 'description', 'due_date')


def home(request):
    return render(request, 'lesTaches/home.html')

def home_name(request, name):
    return render(request, 'lesTaches/home2.html', {'name': name})

def task_listing(request):
    from django.template import Template, Context
    objets = Task.objects.all().order_by('due_date')
    return render(request, 'lesTaches/list.html', {'objets': objets})

def addtask(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('listing'))
    else:
        task = TaskForm()
        return render(request, 'lesTaches/addtask.html', {'task' : task})
