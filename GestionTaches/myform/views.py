from django.shortcuts import render
from django.forms import ModelForm, Textarea
from django.http import HttpResponse, HttpResponseRedirect
from myform.models import Contact, AddTask
from django import forms
from lesTaches.models import Task

# Create your views here.

class ContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ('name', 'firstname', 'email', 'message')

# def contact(request):
#     contact_form = ContactForm()
#     return render(request, 'myform/contact.html', {'contact_form' : contact_form})

class ContactForm2(forms.Form):
    task_name = forms.CharField(label='Task name',max_length=200)
    name = forms.CharField(label='Description',max_length=20000)
    due_date = forms.DateField(label='Due date')

class TaskForm(ModelForm):
    class Meta:
        model = AddTask
        fields = ('name', 'description', 'due_date')

def addtask(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('listing')
    else:
        task = TaskForm()
        return render(request, 'myform/addtask.html', {'task' : task})
