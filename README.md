1. Installation du virtualenv :
    `virtualenv -p python3 ~/venv3`

2. Lancer le virtualenv
    `source ~/venv3/bin/activate`

3. Installer Django
    `pip install django`

4. Lancer le serveur
    `./manage.py runserver`

5. Connectez vous au site <http://localhost:8000>
